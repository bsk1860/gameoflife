# Game Of Life

### Maven

This apaplication is maven project. You need to use maven to build it.

### Install

You can use your IDE to build the application or in a terminal run the following command from the root of the project
>> mvn clean install

### Run tests

You can run tests via this command
>> mvn test

### Run the application
This application is a Spring Boot application. You can run from the root of the project with this command

>> mvn spring-boot:run

## Use the application
When the application is up. Use a browser and go on this adresse < localhost >:< port >/index.html (it's very likely to be [http://localhost:8080/index.html](http://localhost:8080/index.html) if you are using a standard configuration on your computer) 
When the get to that page, upload a txt file respecting the format for this game, then click on "Next Generation" the see the result