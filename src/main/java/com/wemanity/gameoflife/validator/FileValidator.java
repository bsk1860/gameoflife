package com.wemanity.gameoflife.validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.wemanity.gameoflife.exceptions.FileStorageException;
import com.wemanity.gameoflife.helpers.GridHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FileValidator {

	public static final String FILE_INVALID_TYPE = "file.invalid.type";
	public static final String FILE_HEADER_MALFORMED = "file.header.malformed";
	public static final String FILE_LINE_INVALIDE = "file.line.invalide";
	public static final String FILE_LINE_NUMBER_UNMACH = "file.line.number.unmach";
	public static final String FILE_CELL_VALUE = "file.cell.value";
	private static final String FILE_TXT = "txt";
	
	
	private static final Logger logger = LoggerFactory.getLogger(FileValidator.class);


	public void validate(File file) throws FileStorageException, IOException {
		// file extension txt
		if (!FILE_TXT.equals(getFileExtension(file))) {
			throw new FileStorageException(FILE_INVALID_TYPE);
		}
		// read file
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		// first line contains dimensions
		String firstLine = bufferedReader.readLine();
		int[] dimensions = getDimensions(firstLine);
		int numberOfLine = 0;
		while (bufferedReader.ready()) {
			String line = bufferedReader.readLine();

			// check the line has the length.
			if (line.length() != dimensions[1]) {
				throw new FileStorageException(FILE_LINE_INVALIDE);
			}
			// check the cell value is expected values
			for(char s : line.toCharArray()) {
				if(!GridHelper.isKnownCellCharValue(s)) {
					throw  new FileStorageException(FILE_CELL_VALUE);
				}	
			}
			
 			numberOfLine++;
		}
		// check the number of line is correct.
		if (numberOfLine != dimensions[0]) {
			throw new FileStorageException(FILE_LINE_NUMBER_UNMACH);
		}
		
		logger.debug("the file is valid");

	}

	private String getFileExtension(File file) {
		if (file == null)
			return null;

		if (file.getName().lastIndexOf(".") != -1 && file.getName().lastIndexOf(".") != 0)
			return file.getName().substring(file.getName().lastIndexOf(".") + 1);
		else
			return "";
	}

	private int[] getDimensions(String firstLine) {
		String[] rawDimensions = firstLine.split(" ");
		if(rawDimensions.length != 2) {
			throw new FileStorageException(FILE_HEADER_MALFORMED);
		}
		return new int[] { Integer.parseInt(rawDimensions[0]), Integer.parseInt(rawDimensions[1]) };
	}
}
