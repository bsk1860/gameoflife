package com.wemanity.gameoflife.helpers;

public final class GridHelper {

	public final static char ALIVE_CELL_VALUE = '*';
	public final static char DEAD_CELL_VALUE = '.';

	private GridHelper() {

	}

	public static boolean getBooleanCell(char cell) {
		return ALIVE_CELL_VALUE == cell ? true : false;
	}

	public static char getCharCell(boolean b) {
		return b ? ALIVE_CELL_VALUE : DEAD_CELL_VALUE;
	}

	public static boolean isKnownCellCharValue(char s) {
		return ALIVE_CELL_VALUE == s || DEAD_CELL_VALUE == s;
	}

	public static boolean isTargetInGrid(boolean[][] grid, int row, int column) {
		return row > 0 && row < (grid.length - 1) && column > 0 && column < (grid[0].length - 1);
	}

	public static boolean[][] cloneGrid(boolean[][] grid) {
		boolean[][] result = new boolean[grid.length][grid[0].length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = grid[i][j];
			}
		}
		return result;
	}

}
