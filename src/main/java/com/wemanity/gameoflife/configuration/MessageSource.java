package com.wemanity.gameoflife.configuration;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class MessageSource extends ResourceBundleMessageSource{
	
	public MessageSource() {
		super();
		this.setBasenames("messages/messages");
		this.setUseCodeAsDefaultMessage(true);	
	}
    

}
