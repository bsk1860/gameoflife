package com.wemanity.gameoflife.controller;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import lombok.Getter;
import lombok.Setter;

@ApplicationScope
@Component
public class Grids {
	
	@Getter
	@Setter
	boolean[][] inputGrid = null;
	
	@Getter
	@Setter
	boolean[][] outputGrid = null;

}
