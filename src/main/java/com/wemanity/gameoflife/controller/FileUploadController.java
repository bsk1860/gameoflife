package com.wemanity.gameoflife.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.wemanity.gameoflife.exceptions.FileStorageException;
import com.wemanity.gameoflife.service.interfaces.IFileStorageService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
public class FileUploadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	private IFileStorageService storageSerive;

	public final static String UPLOADS = "uploads";

	@Autowired
	private Grids grids;
	
	@Autowired
	private MessageSource messageSource;

	@CrossOrigin(origins = "http://localhost:8080")
	@PostMapping("/seed")
	public ResponseEntity<String> uploadFile(@RequestParam("fileupload") MultipartFile fileMultiPart) {
		
		if(fileMultiPart == null || fileMultiPart.isEmpty()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(messageSource.getMessage("file.required", null, Locale.FRANCE));
		}
		
		Path root = Paths.get(UPLOADS);
		Path pathToFile = root.resolve(fileMultiPart.getOriginalFilename()).toAbsolutePath();
		try {
			// Create uploads if not exists.
			createUploadFolderIfNotExist(root);

			Files.copy(fileMultiPart.getInputStream(), root.resolve(fileMultiPart.getOriginalFilename()));

			File file = new File(pathToFile.toString());
			fileMultiPart.transferTo(file);

			// transform and store it in session.
			grids.setInputGrid(storageSerive.fileToGrid(file.getAbsolutePath()));
			// delete temporary file
			Files.deleteIfExists(pathToFile);
		} catch (FileStorageException | IOException e) {
			logger.info(e.getStackTrace().toString());
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(messageSource.getMessage(e.getMessage(), null, Locale.FRANCE));
		} finally {
			try {
				Files.deleteIfExists(pathToFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return ResponseEntity.status(HttpStatus.OK).body(messageSource.getMessage("upload.ok", null, Locale.FRANCE));
	}
	
	private void createUploadFolderIfNotExist(Path root) throws IOException {
		if (Files.notExists(root, LinkOption.NOFOLLOW_LINKS)) {
			Files.createDirectory(root);
		}
	}
}
