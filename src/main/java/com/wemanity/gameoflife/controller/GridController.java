package com.wemanity.gameoflife.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wemanity.gameoflife.exceptions.FileStorageException;
import com.wemanity.gameoflife.service.interfaces.IFileStorageService;
import com.wemanity.gameoflife.service.interfaces.IGameService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@AllArgsConstructor
public class GridController {

	private static final Logger logger = LoggerFactory.getLogger(GridController.class);

	@Autowired
	public IGameService gameService;

	@Autowired
	public Grids grids;

	@Autowired
	public IFileStorageService fileStorageService;

	@GetMapping(path = "initGrid")
	public boolean[][] getInitialGrip() throws FileStorageException, IOException {
		logger.debug("initGrid called");
		return grids.getInputGrid();
	}

	@GetMapping(path = "endGrid")
	public boolean[][] getEndGrip() throws FileStorageException, IOException {
		logger.debug("endGrid called");
		return gameService.getFinalGrid(grids.getInputGrid());

	}
}
