package com.wemanity.gameoflife.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class ResponseMessage {
	
	@Getter
	@Setter
	private String message;

}
