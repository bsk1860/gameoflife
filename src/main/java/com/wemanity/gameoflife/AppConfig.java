package com.wemanity.gameoflife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppConfig {

	/**
	 * Configure the application and launch it.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

}
