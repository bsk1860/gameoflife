package com.wemanity.gameoflife.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wemanity.gameoflife.exceptions.FileStorageException;
import com.wemanity.gameoflife.helpers.GridHelper;
import com.wemanity.gameoflife.service.interfaces.IFileStorageService;
import com.wemanity.gameoflife.validator.FileValidator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class FileStorageService implements IFileStorageService {

	private static final Logger logger = LoggerFactory.getLogger(FileStorageService.class);

	@Autowired
	private FileValidator fileValidator;

	@Override
	public boolean[][] fileToGrid(String fileName) throws FileStorageException, IOException {
		File file = new File(fileName);
		fileValidator.validate(file);
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

		int[] dimensions = getDimensions(bufferedReader.readLine());
		boolean[][] grid = new boolean[dimensions[0]][dimensions[1]];

		int lineIndex = 1;
		while (bufferedReader.ready()) {
			char[] line = bufferedReader.readLine().toCharArray();
			for (int i = 0; i < line.length; i++) {
				grid[lineIndex-1][i] = GridHelper.getBooleanCell(line[i]);
			}
			lineIndex++;
		}
		
		return grid;
	}
	
	private int[] getDimensions(String firstLine) {
		String[] rawDimensions = firstLine.split(" ");
		return new int[] { Integer.parseInt(rawDimensions[0]), Integer.parseInt(rawDimensions[1]) };
	}
}
