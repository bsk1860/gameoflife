package com.wemanity.gameoflife.service.impl;

import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.wemanity.gameoflife.helpers.GridHelper;
import com.wemanity.gameoflife.service.interfaces.IGameService;

@Service
public class GameService implements IGameService {

	@Override
	public boolean[][] getFinalGrid(boolean[][] grid) {
		boolean[][] result = GridHelper.cloneGrid(grid);

		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				boolean cellAlive = result[i][j];
				if (cellAlive) {

					int numberOfAliveNeighbours = numberOfNeighbors(result, i, j, true);

					if (numberOfAliveNeighbours < 2) {
						// if it has < 2 alive neighbors
						killCell(result, i, j);
					} else if (numberOfAliveNeighbours > 3) {
						// if it has > 3 alive neighbors
						wakeCell(result, i, j);
					}

				} else {
					if (numberOfNeighbors(grid, i, j, true) == 3) {
						// if Alive neighbors numbers == 3
						wakeCell(result, i, j);
					}
				}
			}
		}

		return result;
	}

	private void killCell(boolean[][] grid, int i, int j) {
		grid[i][j] = false;
	}

	private void wakeCell(boolean[][] grid, int i, int j) {
		grid[i][j] = true;
	}

	private int numberOfNeighbors(boolean[][] grid, int row, int column, boolean alive) {
		int nbNeighbors = 0;

		int[][] neighbors = { { row + 1, column }, { row + 1, column + 1 }, { row + 1, column - 1 },
				{ row - 1, column }, { row - 1, column + 1 }, { row - 1, column - 1 }, { row, column - 1 },
				{ row, column + 1 } };

		for (int[] neighbor : neighbors) {
			if (GridHelper.isTargetInGrid(grid, neighbor[0], neighbor[1]) && grid[neighbor[0]][neighbor[1]] == alive) {
				nbNeighbors++;
			}
		}
		return nbNeighbors;
	}
}
