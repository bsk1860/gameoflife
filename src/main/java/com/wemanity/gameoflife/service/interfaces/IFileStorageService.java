package com.wemanity.gameoflife.service.interfaces;

import java.io.IOException;

import com.wemanity.gameoflife.exceptions.FileStorageException;

public interface IFileStorageService {
	
	boolean[][] fileToGrid(String fileName) throws FileStorageException, IOException;
}
