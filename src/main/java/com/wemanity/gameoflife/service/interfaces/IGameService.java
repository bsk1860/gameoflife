package com.wemanity.gameoflife.service.interfaces;

public interface IGameService {

	/**
	 * Transforms a grid to the result according to the rules of the game
	 * @param grid
	 */
	boolean[][] getFinalGrid(boolean[][] grid);

}
