$("form").submit(function(event) {
	event.preventDefault();
	var data = new FormData($("form")[0]);

	$.ajax({
		data: data,
		type: $(this).attr('method'),
		url: $(this).attr('action'),
		contentType: false,
		processData: false,

		success: function(data) {
			$('#flag-response').html(data);
			$('#flag-response').show();
			$('#flag-response').removeClass("alert-danger");
			$('#flag-response').addClass("alert-success");

			$("#next").prop('disabled', false);
			emptyGrids();
			$.get("/initGrid", function(d) {
				$("#inputGridContainer").show();
				displayGrid(d, $("#inputGrid"));
			})
		},
		error: function(data) {
			$("#next").prop('disabled', true);
			emptyGrids();
			$('#flag-response').html(data.responseText);
			$('#flag-response').show();
			$('#flag-response').removeClass("alert-success");
			$('#flag-response').addClass("alert-danger");
		}
	});
});

$("#submit").on("click", function() {
	$("form").submit();
});

$("#next").on("click", function() {
	$('#outputGrid').empty();
	$("#outputGridContainer").hide();
	
	$.get("/endGrid", function(data) {
		$("#outputGridContainer").show();
		displayGrid(data, $("#outputGrid"));
	});
});

function displayCell(data, selector) {
	if (data) {
		selector.append($(".alivecell").html());
	} else {
		selector.append($(".deadcell").html());
	}
}

function newLine(selector) {
	selector.append("<br/>");
}

function displayGrid(data, selector) {
	selector.show();
	if (Array.isArray(data)) {
		data.forEach(function(line) {
			line.forEach(function(cell) {
				displayCell(cell, selector);
			});
			newLine(selector);
		});
	}
}

function emptyGrids() {
	$('#inputGrid').empty();
	$("#inputGridContainer").hide();

	$('#outputGrid').empty();
	$("#outputGridContainer").hide();
}