package com.wemanity.gameoflife.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wemanity.gameoflife.exceptions.FileStorageException;
import com.wemanity.gameoflife.service.impl.FileStorageService;
import com.wemanity.gameoflife.validator.FileValidator;

@ExtendWith(MockitoExtension.class)
public class FileServiceStorageTest {

	@Mock
	private FileValidator fileValidator;

	@InjectMocks
	private FileStorageService fileStorageService;

	/**
	 * Make sure the file is valid before converting to a grid.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testfileToGridValidatesFile() throws IOException {
		fileStorageService.fileToGrid("src/test/resources/test-to-boolean.txt");

		verify(fileValidator, times(1)).validate(Mockito.any(File.class));
	}

	/**
	 * Check grid creating
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testLineTransformationToArrayBooleans() throws FileStorageException, IOException {
		boolean[][] grid = fileStorageService.fileToGrid("src/test/resources/test-to-boolean.txt");

		
		Assertions.assertEquals(2, grid.length);
		Assertions.assertEquals(8, grid[0].length);
		for (int i = 0; i < grid[0].length; i++) {
			if (i == 4) {
				Assertions.assertEquals(true, grid[0][i]);
			} else {
				Assertions.assertEquals(false, grid[0][i]);
			}
		}

	}

}
