package com.wemanity.gameoflife.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wemanity.gameoflife.helpers.GridHelper;
import com.wemanity.gameoflife.service.impl.GameService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

	
	private static final Logger log = LoggerFactory.getLogger(GameServiceTest.class);


	@InjectMocks
	private GameService gameService;

	@Test
	public void testGetNextGridGeneration() {
		// see test.txt
		boolean[][] grid = new boolean[4][8];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				grid[i][j] = false;
			}
		}
		grid[1][4] = true;
		grid[2][3] = true;
		grid[2][4] = true;

		boolean[][] result = gameService.getFinalGrid(grid);

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				if ((i == 1 && j == 4) || (i == 2 && j == 4) || (i == 1 && j == 3) || (i == 2 && j == 3)) {
					Assertions.assertTrue(result[i][j]);
				} else {
					Assertions.assertFalse(result[i][j]);
				}
			}
		}
	}

}
