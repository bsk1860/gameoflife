package com.wemanity.gameoflife.validator;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wemanity.gameoflife.exceptions.FileStorageException;

@ExtendWith(MockitoExtension.class)
public class FileValidatorTest {

	@InjectMocks
	private FileValidator fileValidator;

	/**
	 * Check the extension is correct.
	 * 
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testValidateExtension() throws FileStorageException, IOException {
		Assertions.assertThrows(FileStorageException.class,
				() -> fileValidator.validate(new File("src/test/resources/test-invalid-header.pdf")),
				FileValidator.FILE_INVALID_TYPE);
	}

	/**
	 * check the header is correct
	 * 
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testValidateHeader() throws FileStorageException, IOException {
		Assertions.assertThrows(FileStorageException.class,
				() -> fileValidator.validate(new File("src/test/resources/test-invalid-header.pdf")),
				FileValidator.FILE_HEADER_MALFORMED);

	}

	/**
	 * Check the line has the right number of rows
	 * 
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testValidateLineLenght() throws FileStorageException, IOException {
		Assertions.assertThrows(FileStorageException.class,
				() -> fileValidator.validate(new File("src/test/resources/test-invalid-line.pdf")),
				FileValidator.FILE_LINE_INVALIDE);
	}

	/**
	 * Check the file has the right number of lines
	 * 
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testValidateLineNumber() throws FileStorageException, IOException {
		Assertions.assertThrows(FileStorageException.class,
				() -> fileValidator.validate(new File("src/test/resources/test-too-much-lines.pdf")),
				FileValidator.FILE_LINE_NUMBER_UNMACH);
	}

	/**
	 * Check the file cells have the authorized values
	 * 
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testValidateCellsValue() throws FileStorageException, IOException {
		Assertions.assertThrows(FileStorageException.class,
				() -> fileValidator.validate(new File("src/test/resources/test-cell-value.pdf")),
				FileValidator.FILE_CELL_VALUE);
	}

	/**
	 * check the file is valid
	 * 
	 * @throws FileStorageException
	 * @throws IOException
	 */
	@Test
	public void testValidateFile() throws FileStorageException, IOException {
		fileValidator.validate(new File("src/test/resources/test.txt"));
	}

}
